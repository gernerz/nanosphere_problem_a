/**
 * File: 	flags.cpp
 * Author:	Zach Gerner
 * Created:	4/25/16
 * Description:	
 * 	Flags is a thread-safe class that keeps track of flags. In this simple implementation
 * 	only a quit flag is implemented.
 */

#include "flags.hpp"

Flags::Flags()
{
  quit = false;
  pthread_mutex_init(&quitMutex, 0);
}

Flags::~Flags()
{
  pthread_mutex_destroy(&quitMutex);
}

// Sets the status of the quit flag
void Flags::setQuit()
{
  pthread_mutex_lock(&quitMutex);
  this->quit = true;
  pthread_mutex_unlock(&quitMutex);
}

// Clears the status of the quit flag
void Flags::clearQuit()
{
  pthread_mutex_lock(&quitMutex);
  this->quit = false;
  pthread_mutex_unlock(&quitMutex);
}

// Returns the status of the quit flag
bool Flags::getQuit()
{
  return quit;
}