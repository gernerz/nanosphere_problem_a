/**
 * File: 	flags.hpp
 * Author:	Zach Gerner
 * Created:	4/25/16
 * Description:	
 * 	Flags is a thread-safe class that keeps track of flags. In this simple implementation
 * 	only a quit flag is implemented.
 */

#ifndef FLAGS_H
#define FLAGS_H

#include <pthread.h>

class Flags 
{
private:
  bool quit;;
  pthread_mutex_t quitMutex;
  
public:
  Flags();
  ~Flags();
  
  // Sets the status of the quit flag
  void setQuit();
  
  // Clears the status of the quit flag
  void clearQuit();
  
  // Returns the status of the quit flag
  bool getQuit();
 };

#endif