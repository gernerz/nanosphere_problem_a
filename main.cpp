/**
 * File: 	main.cpp
 * Author:	Zach Gerner
 * Created:	4/25/16
 * Description:	
 * 	This file is the driver for the Problem A of the Nanosphere Software 
 * 	Questions. It first initializes the shared IPC data. Second it starts 
 * 	an instance of threadA and threadB. Finally it waits for both thread to
 * 	complete. As a last step, it cleans up any memory that was used.
 */

#include <iostream>
#include <pthread.h>
#include <stdlib.h>
#include "threadA.hpp"
#include "threadB.hpp"
#include "flags.hpp"
#include "threadComms.hpp"

int main() 
{
    pthread_t threadA;
    pthread_t threadB;
    
    //Configure IPC structures
    threadComms* threadComms = newThreadComms();
    
    //Start threads A & B
    int threadASuccess = pthread_create(&threadA, NULL, threadAStart, (void*)threadComms);
    int threadBSuccess = pthread_create(&threadB, NULL, threadBStart, (void*)threadComms);
    
    //Make sure the threads started successfully
    if(threadASuccess != 0 || threadBSuccess != 0)
    {
      std::cout << "Failed to create threads. Terminating!\n";
      
      //Cleanup
      destroyThreadComms(threadComms);
      exit(EXIT_FAILURE);
    }
    
    //Wait for threads A & B to complete
    pthread_join(threadA, NULL);
    pthread_join(threadB, NULL);
    
    // Cleanup
    destroyThreadComms(threadComms);
    return 0;
}

