/**
 * File: 	threadA.cpp
 * Author:	Zach Gerner
 * Created:	4/25/16
 * Description:	
 * 	ThreadA is a thread that waits for a message from the Message Queue "/AtoBQueue". When a message
 * 	arrives Thread A prints a message to stdout. If the shared Quit flag is set, the thread will exit.
 */

#include <iostream>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "flags.hpp"
#include <mqueue.h>
#include "threadComms.hpp"

#define MaxMsgSize 	100
#define SmallDelay	500

void* threadAStart(void* args)
{
  std::cout << "ThreadA: I am thread A\n";
  
  //Setup IPC
  pthread_mutex_t* critMutex = &(((threadComms*) args)->critMutex);
  Flags* threadFlags = ((threadComms*) args)->flags;
  char buffer[MaxMsgSize + 1];
  int bytesRead = 0;
  mq_attr* mqAttr = ((threadComms*) args)->mqAttr;
  
  mqd_t mq = mq_open("/AtoBQueue", O_RDONLY | O_CREAT | O_NONBLOCK, 0644, mqAttr);
  if (mq == -1)
  {
      std::cout << "Failed to open Message Queue in Thread A: "
		<< strerror(errno) << "\n";
      mq_close(mq);
      pthread_exit(NULL);
  }
  
  //Work until quit flag is set
  std::cout << "ThreadA: I am waiting to get a quit signal\n";
  while(!threadFlags->getQuit())
  {
    pthread_mutex_lock(critMutex);
    bytesRead = mq_receive(mq, buffer, MaxMsgSize, NULL);
    pthread_mutex_unlock(critMutex);
    if(bytesRead > 0)
    {
      std::cout << "Thread A got: ";
      for(int i = 0; i < MaxMsgSize; i++)
      {
	if(buffer[i] != '\n')
	  std::cout << buffer[i];
	else
	  break;
      }
      std::cout << "\n";
    }
    usleep(SmallDelay);
  }
  std::cout << "ThreadA: I got the signal to quit\n";
  
  mq_close(mq);
  pthread_exit(NULL);
}