/**
 * File: 	threadA.hpp
 * Author:	Zach Gerner
 * Created:	4/25/16
 * Description:	
 * 	ThreadA is a thread that waits for a message from the Message Queue "/AtoBQueue". When a message
 * 	arrives Thread A prints a message to stdout. If the shared Quit flag is set, the thread will exit.
 */

#ifndef THREADA_H
#define THREADA_H

void* threadAStart(void* args);

#endif