/**
 * File: 	threadB.cpp
 * Author:	Zach Gerner
 * Created:	4/25/16
 * Description:	
 * 	ThreadB is a thread that will do several operations a few times. First it will print a message to stdout several times. 
 * 	After each message is printed to stdout, it will also send a message via the "/AtoBQueue" Message Queue. Lastly, it will
 * 	sleep for 1 second. 
 * 	After this process has been completed several times, it will set a shared Quit flag and then exit.
 */

#include <iostream>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "threadComms.hpp"
#include "flags.hpp"

#define OneSecondDelay	1000000
#define MaxMsgSize 	100

void* threadBStart(void* args)
{
  std::cout << "ThreadB: I am thread B\n";
  
  //Setup IPC
  pthread_mutex_t* critMutex = &(((threadComms*) args)->critMutex);
  Flags* threadFlags = ((threadComms*) args)->flags;
  int bytesRead = 0;
  mq_attr* mqAttr = ((threadComms*) args)->mqAttr;
  
  mqd_t mq = mq_open("/AtoBQueue", O_WRONLY);
  if (mq == -1)
  {
      std::cout << "Failed to open Message Queue in Thread B: "
		<< strerror(errno) << "\n";
      mq_close(mq);
      pthread_exit(NULL);
  }
  
  for(int i = 0; i < 5; i++)
  {
    std::cout << "Hello from thread B\n";
    pthread_mutex_lock(critMutex);
    mq_send(mq, "HI from B\n", MaxMsgSize, 0);
    pthread_mutex_unlock(critMutex);
    usleep(OneSecondDelay);
  }
  
  std::cout << "ThreadB: I am setting the quit flag\n";
  threadFlags->setQuit();
  
  pthread_exit(NULL);
}