/**
 * File: 	threadB.hpp
 * Author:	Zach Gerner
 * Created:	4/25/16
 * Description:	
 * 	ThreadB is a thread that will do several operations a few times. First it will print a message to stdout several times. 
 * 	After each message is printed to stdout, it will also send a message via the "/AtoBQueue" Message Queue. Lastly, it will
 * 	sleep for 1 second. 
 * 	After this process has been completed several times, it will set a shared Quit flag and then exit.
 */

#ifndef THREADB_H
#define THREADB_H

void* threadBStart(void* args);

#endif