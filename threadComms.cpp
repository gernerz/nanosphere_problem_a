/**
 * File: 	threadComms.cpp
 * Author:	Zach Gerner
 * Created:	4/25/16
 * Description:	
 * 	ThreadComms is a struct of IPC structures and related operations used to communicated between
 * 	multiple threads.
 */

#include "threadComms.hpp"

#define MaxMsgSize 	100
#define MaxMsgCount	10

// Creates a new blank threadComms 
threadComms* newThreadComms()
{
  //Initialize Flags
  threadComms* threadComms = new struct threadComms();
  threadComms->flags = new Flags();
  
  //Initialize Message Queue
  mq_attr* attr = new mq_attr();
  attr->mq_flags = 0;
  attr->mq_maxmsg = MaxMsgCount;
  attr->mq_msgsize = MaxMsgSize;
  attr->mq_curmsgs = 0;
  threadComms->mqAttr = attr;
  
  //Init critMutex
  pthread_mutex_init(&(threadComms->critMutex), 0);
  
  return threadComms;
}

//Destroys the passed threadComms
void destroyThreadComms(threadComms* threadComms)
{
  pthread_mutex_destroy(&(threadComms->critMutex));
  delete threadComms->flags;
  delete threadComms->mqAttr;
  delete threadComms;
  
}


