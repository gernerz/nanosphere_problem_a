/**
 * File: 	threadComms.hpp
 * Author:	Zach Gerner
 * Created:	4/25/16
 * Description:	
 * 	ThreadComms is a struct of IPC structures and related operations used to communicated between
 * 	multiple threads.
 */

#ifndef THREADCOMMS_H
#define THREADCOMMS_H

#include <mqueue.h>
#include "flags.hpp"

struct threadComms
{
  Flags* flags;
  mq_attr* mqAttr;
  pthread_mutex_t critMutex;
};

// Creates a new blank threadComms 
threadComms* newThreadComms();

//Destroys the passed threadComms
void destroyThreadComms(threadComms*);

#endif
